## novel-mobile v1.0
曾经一度痴迷于看小说，小说给了我第二个世界，脱离于现实生活的世界。虽然因为看小说耽误了很多光阴，但是它也给我带来了很多的快乐，伴随主人公的开心而开心，伴随主人公的伤心而伤心。  
只是现在看小说相比之前更为麻烦了，各种书荒，各种收费亦或者各种广告，所以萌生了开发novel的想法，旨在不侵犯他的前提下满足自己开开心心看小说的愿望。  

### auth
admin@nightc.com

### demo
演示地址： [http://novel.nightc.com/](http://novel.nightc.com/)

### TODO v1.1
 - 添加会员登陆以及小说收藏
 - 小说阅读器优化
 - 添加分享功能，二维码生成功能

### how to run
```
git clone https://git.oschina.net/cnjack/novel-mobile
cd novel-mobile
npm(or cnpm) install
npm run dev (开发)
npm run build (构建)
```

## 最新功能
 * 2017-08-30 添加小说阅读界面添加左右键监听 方便PC用户
 * 2017-10-* 添加缓存清楚 解决阅读页面bug 添加版权说明
 * 2017-11-* fix章节加载bug
